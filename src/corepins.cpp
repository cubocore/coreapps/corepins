/*
	*
	* This file is a part of CorePins.
	* A bookmarking app for C Suite.
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


#include <QFile>
#include <QFileDialog>
#include <QMimeData>
#include <QMouseEvent>
#include <QMessageBox>
#include <QScroller>

#include <cprime/sortfunc.h>
#include <cprime/messageengine.h>
#include <cprime/themefunc.h>
#include <cprime/pinit.h>
#include <cprime/appopenfunc.h>
#include <cprime/variables.h>
#include <cprime/filefunc.h>

#include "corepins.h"
#include "ui_corepins.h"


corepins::corepins(QWidget *parent): QWidget(parent)
  , ui(new Ui::corepins)
  , smi(new settings)
{
	ui->setupUi(this);

	QPalette pltt(palette());
	pltt.setColor(QPalette::Base, Qt::transparent);
	setPalette(pltt);

	loadSettings();
	startSetup();
    setupIcons();

//    pm.checkPins();
	sectionRefresh();
	ui->pinlist->viewport()->installEventFilter(this);
}

corepins::~corepins()
{
	delete smi;
	delete ui;
}

/**
 * @brief Setup ui elements
 */
void corepins::startSetup()
{
	// all toolbuttons icon size in sideView
	QList<QToolButton *> toolBtns = ui->sideView->findChildren<QToolButton *>();
	for (QToolButton *b: toolBtns) {
		if (b) {
			b->setIconSize(toolsIconSize);
		}
	}
	ui->menu->setIconSize(toolsIconSize);

	ui->pinlist->setIconSize(listViewIconSize);
	ui->pinlist->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	ui->section->setIconSize(toolsIconSize);

	QScroller::grabGesture(ui->section, QScroller::LeftMouseButtonGesture);
	QScroller::grabGesture(ui->pinlist, QScroller::LeftMouseButtonGesture);

	ui->menu->setVisible(0);
	ui->appTitle->setAttribute(Qt::WA_TransparentForMouseEvents);
	ui->appTitle->setFocusPolicy(Qt::NoFocus);
	this->resize(800, 500);

	if (uiMode != 0) {
		this->setWindowState(Qt::WindowMaximized);

		if (uiMode == 2){
			connect(ui->menu, &QToolButton::clicked, this, &corepins::showSideView);
			connect(ui->appTitle, &QToolButton::clicked, this, &corepins::showSideView);

			ui->sideView->setVisible(0);
			ui->menu->setVisible(1);
		}
	}

    if (uiMode == 2) {
        // setup mobile UI
        this->setWindowState(Qt::WindowMaximized);

        connect(ui->menu, &QToolButton::clicked, this, &corepins::showSideView);
        connect(ui->appTitle, &QToolButton::clicked, this, &corepins::showSideView);
        connect(ui->pinlist, SIGNAL(itemClicked(QTableWidgetItem *)), this, SLOT(openSelectedBookmark(QTableWidgetItem *)));

        ui->sideView->setVisible(0);
        ui->menu->setVisible(1);
    } else {
        // setup desktop or tablet UI

        connect(ui->pinlist, SIGNAL(itemDoubleClicked(QTableWidgetItem *)), this, SLOT(openSelectedBookmark(QTableWidgetItem *)));

        if(windowMaximized){
            this->setWindowState(Qt::WindowMaximized);
            qDebug() << "window is maximized";
        } else{
            this->resize(windowSize);
        }
    }

	bool count = false;
	int c = pm.getPinSections().count();

	// When no section added but speed dial contains item.
	if (c == 1 && pm.getPinNames("Speed Dial").count() >= 1) {
		count = true;
	}

	// When speed dial contains no item but other section do.
	if (c > 1) {
		foreach (QString sec, pm.getPinSections()) {
			if (pm.getPinNames(sec).count()) {
				count = true;
				break;
			}
		}
	}

	if (count) {
		ui->page->setCurrentIndex(1);
	} else {
		ui->page->setCurrentIndex(0);
	}

	ui->pinEditBox->setVisible(false);
	ui->addSectionBox->setVisible(false);
	ui->pinlist->setFocusPolicy(Qt::NoFocus);

	qDebug() << "Pin File: " << CPrime::Variables::CC_PinsFilePath();
	watcher.addPath(CPrime::Variables::CC_PinsFilePath());
	connect(&watcher, &QFileSystemWatcher::fileChanged, this, &corepins::sectionRefresh);
}

void corepins::setupIcons()
{
    ui->addSection->setIcon(CPrime::ThemeFunc::themeIcon( "address-book-new-symbolic", "address-book-new", "address-book-new" ));
    ui->addPin->setIcon(CPrime::ThemeFunc::themeIcon( "bookmark-new-symbolic", "bookmark-new-symbolic", "bookmark-new" ));
    ui->deleteSection->setIcon(CPrime::ThemeFunc::themeIcon( "edit-delete-symbolic", "edit-delete", "edit-delete" ));
    ui->menu->setIcon(CPrime::ThemeFunc::themeIcon( "open-menu-symbolic", "application-menu", "open-menu" ));
}

/**
 * @brief Loads application settings
 */
void corepins::loadSettings()
{
    // get CSuite's settings
    uiMode = smi->getValue("CoreApps", "UIMode");
    toolsIconSize = smi->getValue("CoreApps", "ToolsIconSize");
    listViewIconSize = smi->getValue("CoreApps", "ListViewIconSize");

    // get app's settings
    windowSize = smi->getValue("CorePins", "WindowSize");
    windowMaximized = smi->getValue("CorePins", "WindowMaximized");

    bAddPin = false;
}

void corepins::showSideView()
{
	if (ui->sideView->isVisible())
		ui->sideView->setVisible(0);
	else
		ui->sideView->setVisible(1);
}

void corepins::changeState(bool state)
{
	ui->section->setEnabled(state);
	ui->searchPins->setEnabled(state);
	ui->pinlist->setEnabled(state);
}

void corepins::sectionRefresh()
{
	qDebug() << "Refreshing view...";
	QString searchStr = ui->searchPins->text();
	ui->searchPins->clear();
	ui->pinlist->clear();
	ui->searchPins->clear();

	int selectedIndex = ui->section->currentIndex().row();

	ui->section->clear();
	ui->section->addItems(pm.getPinSections());

    for (int i = 0; i < ui->section->count(); ++i) {
        ui->section->item(i)->setIcon(CPrime::ThemeFunc::themeIcon( "x-office-address-book-symbolic", "x-office-address-book", "address-book" ));
	}

	ui->section->setCurrentRow(selectedIndex);

	if (selectedIndex > -1) {
		on_section_itemClicked(ui->section->currentItem());
	} else if (selectedIndex == -1) {
		ui->section->setCurrentRow(0);
		on_section_itemClicked(ui->section->currentItem());
	}

	ui->searchPins->setText(searchStr);
	watcher.addPath(CPrime::Variables::CC_PinsFilePath());
}

void corepins::on_selectSection_currentIndexChanged(const QString &arg1)
{
	Q_UNUSED(arg1)
	on_pinPath_textChanged(ui->pinPath->text());
	on_pinName_textChanged(ui->pinName->text());
}

void corepins::on_section_itemClicked(QListWidgetItem *item)
{
	// first do this to get all the space for contents
	if (uiMode == 2){
		ui->sideView->setVisible(0);
	}


	if (ui->section->currentItem()->text() == "Speed Dial") {
		ui->deleteSection->setEnabled(false);
	} else {
		ui->deleteSection->setEnabled(true);
	}

	this->setWindowTitle(item->text() + " - CorePins");

	QStringList list = pm.getPinNames(item->text());
	int count = list.count();

	ui->pinlist->clear();

	ui->pinlist->setColumnCount(3);
	ui->pinlist->setRowCount(count);

	QStringList dateTimeList;

	for (int i = 0; i < count; i++) {
		dateTimeList.append(pm.piningTime(ui->section->currentItem()->text(), list.at(i)));
	}

	dateTimeList = CPrime::SortFunc::sortDateTime(dateTimeList, Qt::DescendingOrder, "hh.mm.ss - dd.MM.yyyy");
	QStringList mList;
	mList.clear();

	for (int i = 0; i < count; i++) {
		for (int j = 0; j < count; j++) {
			QString bTime = pm.piningTime(ui->section->currentItem()->text(), list.at(j));

			if (bTime.contains(dateTimeList.at(i))) {
				mList.append(list.at(j));
			}
		}
	}

	dateTimeList.clear();
	list.clear();

	QTableWidgetItem *pinItem;

	for (int i = 0; i < count; i++) {
		pinItem = new QTableWidgetItem(mList.at(i));
		QIcon icon = CPrime::ThemeFunc::getFileIcon(pm.pinPath(ui->section->currentItem()->text(), mList.at(i)));
		QSize cSize = ui->pinlist->iconSize();          // Correct size
		QSize iSize = icon.actualSize(cSize);           // iconSize

		if (iSize.width() > cSize.width() or iSize.height() > cSize.height()) {
			icon = QIcon(icon.pixmap(cSize).scaled(cSize, Qt::KeepAspectRatio, Qt::SmoothTransformation));
		}

		pinItem->setIcon(icon);

		ui->pinlist->setItem(i, 0, pinItem);
		ui->pinlist->setItem(i, 1, new QTableWidgetItem(pm.pinPath(ui->section->currentItem()->text(), mList.at(i))));

		pinCellItemWidget *itemWid = new pinCellItemWidget(pinItem, smi, this);
		ui->pinlist->setCellWidget(i, 2, itemWid);

		connect(itemWid, &pinCellItemWidget::editOccurred, this, &corepins::pinEdit);
		connect(itemWid, &pinCellItemWidget::removeOccurred, this, &corepins::pinDelete);
	}

	int s = ui->pinlist->horizontalHeader()->size().width();
	int j = static_cast<int>(s * .1);
	ui->pinlist->horizontalHeader()->setMinimumSectionSize(j);
	ui->pinlist->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
	ui->pinlist->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
	ui->pinlist->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Fixed);
}

void corepins::on_addSection_clicked()
{
	// first do this to get all the space for contents
	if (uiMode == 2){
		ui->sideView->setVisible(0);
	}

	if (ui->pinEditBox->isVisible()) {
		ui->pinEditBox->setVisible(false);
		ui->selectSection->clear();
	}

	ui->addSectionBox->setVisible(true);
	ui->sectionDone->setEnabled(false);
}

void corepins::on_deleteSection_clicked()
{
	// first do this to get all the space for contents
	if (uiMode == 2){
		ui->sideView->setVisible(0);
	}

	QMessageBox message(QMessageBox::Question, tr("Delete Section"), "Do you want to delete this section?", QMessageBox::No | QMessageBox::Yes);
	message.setWindowIcon(QIcon("corepins"));

	int merge = message.exec();

	if (merge == QMessageBox::Yes) {
		pm.delSection(ui->section->currentItem()->text());
		ui->section->takeItem(ui->section->currentIndex().row());
	}

	sectionRefresh();
}

void corepins::on_addPin_clicked()
{
	// first do this to get all the space for contents
	if (uiMode == 2){
		ui->sideView->setVisible(0);
	}

	if(ui->addSectionBox->isVisible())
		ui->addSectionBox->setVisible(false);

	if (ui->pinEditBox->isVisible()) {
		ui->pinEditBox->setVisible(false);
		ui->selectSection->clear();
	}


    QString openFrom = QDir::homePath();
    if (!workFilePath.isNull()) {
        openFrom = CPrime::FileUtils::dirName( workFilePath);
    }

    QString fileP = QFileDialog::getOpenFileName(this, tr("Select File..."), openFrom);

    if (fileP.length()) {
        PinIT pit(QStringList() << fileP, this);
		bool done = pit.exec();
		if (done) {
			ui->page->setCurrentIndex(1);
			sectionRefresh();
		} else {
			CPrime::MessageEngine::showMessage(
						"cc.cubocore.CorePins",
						"CorePins",
						"Can't Pin the selected file",
						"File already exists in another section."
			);
        }

        workFilePath = fileP;
	}
}

void corepins::on_sectionDone_clicked()
{
	QString sectionName = ui->sectionName->text();
	pm.addSection(sectionName);

	QListWidgetItem *item = new QListWidgetItem(sectionName);
    item->setIcon(CPrime::ThemeFunc::themeIcon( "address-book-new-symbolic", "address-book-new", "address-book-new" ));
	ui->section->addItem(item);

	ui->sectionName->setText("");
	ui->sectionStatus->setText("");
	ui->addSectionBox->setVisible(false);

}

void corepins::on_sectionCancel_clicked()
{
	ui->sectionName->setText("");
	ui->sectionStatus->setText("");
	ui->addSectionBox->setVisible(false);
}

void corepins::on_sectionName_textChanged(const QString &arg1)
{
    if (arg1.length() > 0) {
		if (pm.getPinSections().contains(arg1, Qt::CaseInsensitive)) {
			ui->sectionStatus->setText("Section Exists");
			ui->sectionDone->setEnabled(false);
		} else {
			ui->sectionStatus->setText("");
			ui->sectionDone->setEnabled(true);
		}
	}

	else if (ui->sectionName->text().isEmpty()) {
		ui->sectionDone->setEnabled(false);
	}
}

void corepins::on_editDone_clicked()
{
	QString pinValue = QDir(ui->pinPath->text()).absolutePath();

	if (bAddPin) {
		pm.addPin(ui->selectSection->currentText(), ui->pinName->text(), ui->pinPath->text());
	} else {
		pm.changeAll(ui->section->currentItem()->text(), ui->pinlist->item(ui->pinlist->currentIndex().row(), 0)->text(),
					 ui->selectSection->currentText(), ui->pinName->text(), pinValue);
	}

	if (ui->section->currentItem()->text() == ui->selectSection->currentText()) {
		if (bAddPin) {
			ui->pinlist->insertRow(1);

			int index = ui->pinlist->rowCount() - 1;
			QString filePath = ui->pinPath->text();
			QIcon icon = CPrime::ThemeFunc::getFileIcon(filePath);
			QSize cSize = ui->pinlist->iconSize();
			QSize iSize = icon.actualSize(cSize);
			if (iSize.width() > cSize.width() or iSize.height() > cSize.height()) {
				icon = QIcon(icon.pixmap(cSize).scaled(cSize, Qt::KeepAspectRatio, Qt::SmoothTransformation));
			}

			QTableWidgetItem *pinItem = new QTableWidgetItem(filePath);
			pinItem->setIcon(icon);

			ui->pinlist->setItem(index, 0, pinItem);
			ui->pinlist->setItem(index, 1, new QTableWidgetItem(filePath));

			pinCellItemWidget *itemWid = new pinCellItemWidget(pinItem, smi, this);
			ui->pinlist->setCellWidget(index, 2, itemWid);

			connect(itemWid, &pinCellItemWidget::editOccurred, this, &corepins::pinEdit);
			connect(itemWid, &pinCellItemWidget::removeOccurred, this, &corepins::pinDelete);
		} else {
			int current = ui->pinlist->currentIndex().row();
			ui->pinlist->item(current, 0)->setText(ui->pinName->text());
			ui->pinlist->item(current, 1)->setText(QDir(ui->pinPath->text()).absolutePath());
		}
	} else {
		if (bAddPin) {
			// Nothing to do
		} else {
			ui->pinlist->removeRow(ui->pinlist->currentRow());
		}
	}

	sectionRefresh();
	on_editCancel_clicked();

	changeState(true);
}

void corepins::on_editCancel_clicked()
{
	ui->pinEditBox->setVisible(false);
	ui->selectSection->clear();
	ui->pinName->clear();
	ui->statuslbl->clear();
	ui->pinPath->clear();

	changeState(true);
}

void corepins::openSelectedBookmark(QTableWidgetItem *item)
{
	QString path = pm.pinPath(ui->section->currentItem()->text(), ui->pinlist->item(item->row(), 0)->text());

	CPrime::AppOpenFunc::appOpenEngine(path);
}

void corepins::reload()
{
	sectionRefresh();
}

void corepins::sendFiles(const QStringList &paths)
{
	PinIT pit(paths, this);
	pit.exec();
}

void corepins::pinEdit(QTableWidgetItem *item)
{
	ui->pinlist->setCurrentItem(item);

	ui->pinEditBox->setVisible(true);
	ui->selectSection->clear();
	ui->selectSection->addItems(pm.getPinSections());
	ui->selectSection->setCurrentText(ui->section->currentItem()->text());
	ui->pinName->setText(ui->pinlist->item(ui->pinlist->currentIndex().row(), 0)->text());
	ui->pinPath->setText(ui->pinlist->item(ui->pinlist->currentIndex().row(), 1)->text());
	bAddPin = false;

	if(ui->addSectionBox->isVisible())
		ui->addSectionBox->setVisible(0);

	changeState(false);
}

void corepins::pinDelete(QTableWidgetItem *item)
{
	if (!item) {
		qDebug() << "Severe problem. No item found.";
		return;
	}

	ui->pinlist->setCurrentItem(item);

	QMessageBox message(QMessageBox::Question, tr("Delete Pin"), "Do you want to delete the pin?", QMessageBox::No | QMessageBox::Yes);
	message.setWindowIcon(QIcon("corepins"));

	if (ui->pinEditBox->isVisible()) {
		ui->pinEditBox->setVisible(0);
	}

	int merge = message.exec();

	if (merge == QMessageBox::Yes) {
		pm.delPin(ui->pinlist->currentItem()->text(), ui->section->currentItem()->text());
		int r = ui->pinlist->currentItem()->row();

		ui->pinlist->cellWidget(item->row(), 2)->deleteLater();
		delete item;

		ui->pinlist->removeRow(r);
	}

	sectionRefresh();
}

void corepins::on_pinName_textChanged(const QString &arg1)
{
    if (ui->pinName->text().length() > 0) {
		QString str = pm.checkingPinName(ui->selectSection->currentText(), arg1);

        if (str.length() > 0) {
			ui->statuslbl->setText(str);
			ui->editDone->setEnabled(false);
		} else {
			ui->statuslbl->setText(str);

            if (!ui->pinPath->text().isNull() && ui->pathlbl->text().length() == 0) {
				ui->editDone->setEnabled(true);
			} else {
				ui->editDone->setEnabled(false);
			}
		}
	} else {
		ui->editDone->setEnabled(false);
	}
}

void corepins::on_pinPath_textChanged(const QString &arg1)
{
	QFileInfo info(arg1);

	if (info.isDir()) {
		QDir dir(arg1);

        if (ui->pinPath->text().length() > 0) {
			if (!QDir(dir.absolutePath()).exists()) {
				ui->pathlbl->setText(("Invalid path."));
				ui->editDone->setEnabled(false);
			} else {
				QString str = pm.checkingPinPath(ui->selectSection->currentText(), dir.absolutePath());

                if (str.length() > 0) {
					ui->pathlbl->setText(str);
					ui->editDone->setEnabled(false);
				} else {
					ui->pathlbl->setText(str);

                    if (ui->pinName->text().length() != 0 && ui->statuslbl->text().length() == 0) {
						ui->editDone->setEnabled(true);
					} else {
						ui->editDone->setEnabled(false);
					}
				}
			}
		} else {
			ui->editDone->setEnabled(false);
		}
	} else if (info.isFile()) {
		QFile file(arg1);

        if (ui->pinPath->text().length() > 0) {
			if (!QFile(file.fileName()).exists()) {
				ui->pathlbl->setText(("Invalid file."));
				ui->editDone->setEnabled(false);
			} else {
				QString str = pm.checkingPinPath(ui->selectSection->currentText(), file.fileName());

                if (str.length() > 0) {
					ui->pathlbl->setText(str);
					ui->editDone->setEnabled(false);
				} else {
					ui->pathlbl->setText(str);

                    if (ui->pinName->text().length() != 0 && ui->statuslbl->text().length() == 0) {
						ui->editDone->setEnabled(true);
					} else {
						ui->editDone->setEnabled(false);
					}
				}
			}
		} else {
			ui->editDone->setEnabled(false);
		}
	}
}

void corepins::on_searchPins_textChanged(const QString &arg1)
{
	if (!arg1.isNull()) {
		for (int i = 0; i < ui->pinlist->rowCount(); ++i) {
			if (ui->pinlist->item(i, 0)->text().contains(arg1, Qt::CaseInsensitive)) {
				ui->pinlist->item(i, 0)->setSelected(true);
				ui->pinlist->item(i, 1)->setSelected(true);
			} else {
				ui->pinlist->item(i, 0)->setSelected(false);
				ui->pinlist->item(i, 1)->setSelected(false);
			}
		}
	}

	if (arg1.isEmpty()) {
		ui->pinlist->clearSelection();
	}
}

void corepins::on_btnBrowse_clicked()
{
    QString dir = ui->pinPath->text().length() ? QFileInfo(ui->pinPath->text()).absolutePath() : "";

    if (!dir.length()) {
		dir = QDir::homePath();
	}

	QString filePath = QFileDialog::getOpenFileName(this, "Select File...", dir);

    if (filePath.length()) {
		ui->pinPath->setText(filePath);
	}
}

void corepins::on_startAddPin_clicked()
{
	QString workFilePath = QFileDialog::getOpenFileName(this, "Select File...", QDir::homePath());

    if (workFilePath.length()) {
		PinIT pit(QStringList() << workFilePath, this);
		bool done = pit.exec();
		if (done) {
			ui->page->setCurrentIndex(1);
			sectionRefresh();
		}
	}
}

void corepins::closeEvent(QCloseEvent *cEvent)
{
    qDebug()<< "save window stats"<< this->size() << this->isMaximized();

    smi->setValue("CorePins", "WindowSize", this->size());
    smi->setValue("CorePins", "WindowMaximized", this->isMaximized());

}

pinCellItemWidget::pinCellItemWidget(QTableWidgetItem *container, settings *smi, QWidget *parent)
	: QWidget(parent)
{
	m_container = container;

	QHBoxLayout *lay = new QHBoxLayout;
	QToolButton *btnEdit = new QToolButton;
	QToolButton *btnDelete = new QToolButton;

    btnEdit->setIcon(CPrime::ThemeFunc::themeIcon( "document-edit-symbolic", "edit-rename", "document-edit" ));
	btnEdit->setIconSize(smi->getValue("CoreApps", "ListViewIconSize") - QSize(8,8));
    btnDelete->setIcon(CPrime::ThemeFunc::themeIcon( "edit-delete-symbolic", "edit-delete", "edit-delete" ));
	btnDelete->setIconSize(smi->getValue("CoreApps", "ListViewIconSize") - QSize(8,8));

	lay->addWidget(btnEdit);
	lay->addWidget(btnDelete);

	this->setLayout(lay);

	connect(btnEdit, &QToolButton::clicked, [this]() {
		emit editOccurred(m_container);
	});

	connect(btnDelete, &QToolButton::clicked, [this]() {
		emit removeOccurred(m_container);
	});
}
