/*
    *
    * This file is a part of CorePins.
    * A bookmarking app for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include "corepins.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>

#include <cprime/cprime.h>


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // Set application info
    app.setOrganizationName("CuboCore");
    app.setApplicationName("CorePins");
    app.setApplicationVersion(QStringLiteral(VERSION_TEXT));
    app.setDesktopFileName("cc.cubocore.CorePins.desktop");
    app.setQuitOnLastWindowClosed(true);

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();
	parser.addOption(QCommandLineOption(QStringList() << "pinIt", "Bookmark file"));
	parser.addPositionalArgument("files", "Bookmark file paths");
    parser.process(app);

    QStringList args = parser.positionalArguments();
    QStringList paths;

	Q_FOREACH (QString arg, args) {
        QFileInfo info(arg);
        paths.push_back(info.absoluteFilePath());
    }

	corepins bk;

	if (parser.isSet("pinIt") || paths.count()) {
		bk.sendFiles(paths);
	}

	bk.show();

    return app.exec();
}
